import React, { Component } from 'react';
import Home from './components/home'
import { MContext } from './contextprovider';
class App extends Component {
  state = {
    hint_text: 'Search a country (start with a)'
  }
  render() {
    return (
      <MContext.Provider value={this.state}>
        <Home />
      </MContext.Provider>
    );
  }
}

export default App;
