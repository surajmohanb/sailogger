import browserInfo from 'browser-info';
import uuid from 'uuid/v4';

export const cLogger = (obj) => {
    var eventObject = Object.assign({}, browserInfo(), {
        "BrowserTimestamp": new Date(),
        "Page": "Demo",
        "PageId": uuid(),
        "TrackingId": uuid(),
        "Component": "Home Component",

    }, obj);
    console.log(eventObject)
};