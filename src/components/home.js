import React from 'react';
import PropTypes from 'prop-types';
import Autosuggest from 'react-autosuggest';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import { cLogger } from '../commons/clogger'
import { renderSuggestion } from '../HOC/render_suggestion'
import { renderInputComponent } from '../HOC/render_input_components'
import { getSuggestions } from '../HOC/get_suggestions'
import { MContext } from '../contextprovider'

function getSuggestionValue(suggestion) {
    return suggestion.label;
}

const styles = theme => ({
    root: {
        padding: 20,
        height: 250,
        flexGrow: 1,
        width: 650
    },
    container: {
        position: 'relative',
    },
    suggestionsContainerOpen: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing.unit,
        left: 0,
        right: 0,
    },
    suggestion: {
        display: 'block',
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },
    divider: {
        height: theme.spacing.unit * 2,
    },
});

class Home extends React.Component {
    state = {
        single: '',
        popper: '',
        suggestions: [],
    };

    handleSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: getSuggestions(value),
        });
    };

    handleSuggestionsClearRequested = () => {
        this.setState({
            suggestions: [],
        });
    };

    captureEvent(event) {
        cLogger({
            event: event.type,
            eventComponent: event.target
        })
    }

    handleChange = name => (event, { newValue }) => {
        cLogger({ event: event.type, eventComponent: event.target })
        this.setState({
            [name]: newValue,
        });
    };

    render() {
        const { classes } = this.props;

        const autosuggestProps = {
            renderInputComponent,
            suggestions: this.state.suggestions,
            onSuggestionsFetchRequested: this.handleSuggestionsFetchRequested,
            onSuggestionsClearRequested: this.handleSuggestionsClearRequested,
            getSuggestionValue,
            renderSuggestion,
        };

        return (
            <div className={classes.root}>
                <MContext.Consumer>
                    {(context) => (
                        <React.Fragment>
                            <Autosuggest
                                {...autosuggestProps}
                                inputProps={{
                                    classes,
                                    placeholder: context.hint_text,
                                    value: this.state.single,
                                    onFocus: this.captureEvent,
                                    onMouseOver: this.captureEvent,
                                    onMouseLeave: this.captureEvent,
                                    onChange: this.handleChange('single'),
                                }}
                                theme={{
                                    container: classes.container,
                                    suggestionsContainerOpen: classes.suggestionsContainerOpen,
                                    suggestionsList: classes.suggestionsList,
                                    suggestion: classes.suggestion,
                                }}
                                renderSuggestionsContainer={options => (
                                    <Paper {...options.containerProps} square>
                                        {options.children}
                                    </Paper>
                                )}
                            />
                            <div className={classes.divider} />
                        </React.Fragment>
                    )}

                </MContext.Consumer>
            </div>
        );
    }
}

Home.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Home);